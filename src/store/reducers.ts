import { combineReducers } from 'redux';

/* Feature reducers */
import news from '../features/news/store/reducer';

const rootReducer = combineReducers({
  news
});

export default rootReducer;
