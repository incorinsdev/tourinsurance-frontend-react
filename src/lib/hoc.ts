import { ComponentType } from 'react';

export function getDisplayName<P extends object>(
  WrapperdComponent: ComponentType<P>
): string {
  return WrapperdComponent.displayName || WrapperdComponent.name || 'Component';
}
