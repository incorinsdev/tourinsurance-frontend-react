interface IGeneratedTypes {
  PENDING: string;
  SUCCESS: string;
  ERROR: string;
}

/**
 * Method for generation action types
 *
 * @param type Redux action type
 */
export const generateTypes = (
  namespace: string,
  type: string
): IGeneratedTypes => ({
  ERROR: `${namespace}/${type}_ERROR`,
  PENDING: `${namespace}/${type}_PENDING`,
  SUCCESS: `${namespace}/${type}_SUCCESS`
});
