import classnames from 'classnames';
import React, { Component } from 'react';
import './FaqPage.css';

/* Components */
import { IThemeContext, withTheme } from '../../components';
import PageHeader from '../../components/PageHeader';

class FaqPage extends Component<IThemeContext> {
  public render() {
    const { theme } = this.props;
    const classes = classnames('faq-page', {
      [`faq-page--${theme}`]: theme
    });

    return (
      <section className={classes}>
        <PageHeader title="Вопросы и ответы" />
      </section>
    );
  }
}

export default withTheme(FaqPage);
