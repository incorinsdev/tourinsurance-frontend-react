import classnames from 'classnames';
import React, { Component } from 'react';
import { GeoObject, Map, YMaps } from 'react-yandex-maps';
import './ContactsPage.css';

/* Components */
import { IThemeContext, withTheme } from '../../components';
import PageHeader from '../../components/PageHeader';

class ContactsPage extends Component<IThemeContext> {
  public render() {
    const { theme } = this.props;
    const classes = classnames('contacts-page', {
      [`contacts-page--${theme}`]: theme
    });

    return (
      <section className={classes}>
        <PageHeader title="Контактная информация" />
        <div className="contacts-page__map">
          <YMaps>
            <Map
              width="100%"
              height="100%"
              defaultState={{ center: [55.79, 37.706], zoom: 16 }}
            >
              <GeoObject
                geometry={{
                  coordinates: [55.79, 37.71],
                  type: 'Point'
                }}
              />
            </Map>
          </YMaps>
          <div className="contacts-page__contacts">
            <header className="contacts-page__top">
              <address>г. Москва, ул. Суворовская 10А, оф. 10</address>
            </header>
            <div className="contacts-page__bottom">
              <div className="contacts-page__left">
                <a className="contacts-page__phone" href="tel:+74961182324">
                  8 (495) 118 23 24
                </a>
                <a
                  className="contacts-page__email"
                  href="mailto:welcome@tour-insurance.ru"
                >
                  tour-insurance.ru
                </a>
              </div>
              <div className="contacts-page__right" />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default withTheme(ContactsPage);
