import React, { SFC } from 'react';
import './ErrorPage.css';
// import errors from './errors.json';

const ErrorPage: SFC = () => (
  <section className="error-page">
    <h2>Ошибка</h2>
  </section>
);

export default ErrorPage;
