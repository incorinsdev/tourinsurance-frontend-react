import React, { Component } from 'react';
import './TouristsAboutPage.css';

/* Images */
import imageLizard from '../../assets/images/lizard-up.svg';

/* Components */
import DarkenContainer from '../../components/DarkenContainer';
import DashedContainer from '../../components/DashedContainer';
import IconCup from '../../components/Icons/Cup';
import IconLogo from '../../components/Icons/Logo';
import PageHeader from '../../components/PageHeader';

class TouristsAboutPage extends Component {
  public render() {
    return (
      <section className="tabout-page">
        <PageHeader title="Несколько слов о компании" />
        <div className="tabout-page__about">
          <div className="tabout-page__about-title">
            Компания Туриншуранс – держатель одного из лучших on-line сервисов
            по страхованию имущества (квартиры, дома) на время путешествия.
          </div>
          <div className="tabout-page__about-delimiter" />
          <div className="tabout-page__about-info">
            Мы представляем Продукт, разработанный в сотрудничестве и под
            руководством ведущих экспертов туристической отрасли.
          </div>
        </div>
        <div className="tabout-page__top-delimiter">
          <div className="tabout-page__about-logo">
            <IconLogo />
          </div>
        </div>
        <div className="tabout-page__info">
          <header className="tabout-page__info-title">
            Актуальность страхования квартир на время поездки.
          </header>
          <div className="tabout-page__info-content">
            <div className="tabout-page__info-description">
              Стало аксиомой, что международный и внутренний туризм немыслим
              сегодня без страхования. Страхование туристов в зарубежных
              туристских поездках, как правило, включает оказание туристу
              экстренной медицинской помощи во время зарубежной поездки при
              внезапном заболевании или несчастном случае. Эти страховки являются
              обязательными для поездок во многие страны. Однако во всем
              цивилизованном мире практика имущественного страхования туристов и
              путешественников принята уже давно.
              <br />
              Страховой полис - необходимая составляющая туристского сервиса,
              гарантирующая туристам (путешественникам) безопасность и комфорт,
              постепенно приживается и в России. Более того, на такое страхование
              есть спрос. Настал момент, когда люди стали задумываться про
              страхование своего жилья. Особенно эта услуга актуальна на период
              отпуска или длительного отсутствия - во время отъезда из города.
              Уезжая в отпуск или в длительную командировку, людям не хочется
              переживать, что по приезду в квартире их врасплох застанут
              неприятности.
              <br />
              Обезопасить себя от всего этого можно, застраховав квартиру от
              возможных рисков на период их отсутствия.
            </div>
            <div className="tabout-page__info-right">
              <img src={imageLizard} />
            </div>
          </div>
        </div>
        <DarkenContainer className="tabout-page__statistic">
          <header className="tabout-page__statistic-title">
            По статистике, за 1 год пожары повреждают около 90 000 строений....
          </header>
          <div className="tabout-page__statistic-description">
            При этом большинство пожаров, по понятным причинам, происходит
            именно летом. Также лето – это период отпусков, во время которого
            количество квартирных краж вырастает в 3,2 раза. Почти все плановые
            ремонтные работы на тепломагистралях, не редко приводящие к
            разгерметизации батарей, тоже проводятся летом.
          </div>
          <div className="tabout-page__statistic-info">
            <div className="tabout-page__statistic-subtitle">
              Только 15% жилья в России застраховано.
            </div>
            <div className="tabout-page__statistic-subdescription">
              Нами было опрошено более 2600 человек из разных регионов России и
              выяснилось, что самые большие страхи у туристов, уезжающих на
              отдых были относительно сохранности их жилья, которое они покидают
              на длительное время (даже если будут дома престарелые родители).
            </div>
          </div>
        </DarkenContainer>
        {/* <div className="tabout-page__steps">
          <div className="tabout-page__step">
            <div className="tabout-page__step-left" />
            <div className="tabout-page__step-right">
              <header className="tabout-page__step-title">
                Страхование квартиры на время отпуска – это разумный способ
                сохранить оставленное без присмотра имущество стоимостью в
                несколько миллионов рублей
              </header>
              <div className="tabout-page__step-description">
                заплатив всего 1500 рублей. Если во время отсутствия клиента с
                квартирой или находящимся в ней имуществом что-то случится, он
                получит компенсацию в размере стоимости причиненного ущерба,
                в пределах выбранной страховой суммы.
              </div>
            </div>
          </div>
          <div className="tabout-page__step">
            <div className="tabout-page__step-left" />
            <div className="tabout-page__step-right">
              <header className="tabout-page__step-title">
                По данным проведенного нами опроса 40% туристов заключили бы
                договор страхования имущества, если бы процесс был
                автомтизирован, а стоимость не превышала 2 500 - 3000 рублей.
              </header>
              <div className="tabout-page__step-description">
                Почти все страховые компании предлдагают оформить страхование
                имущества. Стоимость двухнедельного полиса в среднем обойдется
                от 350 до 5 000 рублей в зависимости от наполнения программы..
                <br />
                Выезжающим за границу можно поступить еще проще: во время
                оформления туристической путевки выбрать дополнительную опцию
                «Страхование квартиры».
              </div>
            </div>
          </div>
        </div> */}
        <DashedContainer className="tabout-page__lider">
          <div className="tabout-page__lider-left">
            <header className="tabout-page__lider-title">
              ООО «ТурИншуранс» работает с лидерами Российского страхового рынка.
            </header>
            <div className="tabout-page__lider-description">
              Изучив все предложения от ведущих Российских компаний, мы смогли
              выбрать лучшие предложения по соотношению стоимости и качества,
              которые клиент может приобрети одновременно с туром в турагентстве.
            </div>
          </div>
          <div className="tabout-page__lider-right">
            <IconCup />
          </div>
        </DashedContainer>
        <div className="tabout-page__contacts">
          <div className="tabout-page__contacts-hint">
            Для оформления страховки Вы можете связаться по многоканальному телефону
          </div>
          <div className="tabout-page__contacts-phone">
            <a href="tel:+74951182324">8 (495) 118-23-24</a>
          </div>
          <div className="tabout-page__contacts-email">
            или написать на почту: {' '}
            <a href="mailto:mailto:welcome@tour-insurance.ru">welcome@tour-insurance.ru</a>
          </div>
        </div>
      </section>
    );
  }
}

export default TouristsAboutPage;
