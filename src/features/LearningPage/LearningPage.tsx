import React, { Component } from 'react';
import './LearningPage.css';

/* Components */
import { Button, TextField } from '../../components';
import PageHeader from '../../components/PageHeader';

class LearningPage extends Component {
  public render() {
    return (
      <section className="personal-area-page">
        <PageHeader title="Войдите в систему, для просмотра раздела" />
        <div className="personal-area-page__bottom">
          <form className="personal-area-page__form">
            <div className="personal-area-page__form-top">
              <TextField label="Логин *" value="" placeholder="Введите логин" />
              <TextField label="Пароль *" value="" placeholder="Введите пароль" />
            </div>
            <div className="personal-area-page__form-bottom">
              <div className="personal-area-page__delimiter" />
              <Button label="Войти в систему" />
            </div>
            <div className="personal-area-page__form-hint">
              Если вы забыли пароль, перейдите на форму восстановления пароля
            </div>
          </form>
        </div>
      </section>
    );
  }
}

export default LearningPage;
