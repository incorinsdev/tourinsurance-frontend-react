import React, { Component } from 'react';
import Loadable from 'react-loadable';
import { Route, Switch } from 'react-router-dom';
import './PartnersPage.css';

/* Pages */
const AllNewsPage = Loadable({
  loader: () => import(/* webpackChunkName: "PartnersAllNewsPage"*/ '../news/ListNews'),
  loading: () => <div>Loading...</div>
});

const NewsPage = Loadable({
  loader: () => import(/* webpackChunkName: "PartnersAllNewsPage"*/ '../news/News'),
  loading: () => <div>Loading...</div>
});

const LearningPage = Loadable({
  loader: () => import(/* webpackChunkName: "LearningPage"*/ '../LearningPage'),
  loading: () => <div>Loading...</div>
});

const FaqPage = Loadable({
  loader: () => import(/* webpackChunkName: "FaqPage"*/ '../FaqPage'),
  loading: () => <div>Loading...</div>
});

const ContactsPage = Loadable({
  loader: () => import(/* webpackChunkName: "ContactsPage"*/ '../ContactsPage'),
  loading: () => <div>Loading...</div>
});

/* Components */
import {
  IconHat,
  IconInfo,
  IconMarker,
  IconMegaphone,
  IconMessage,
  IconPen,
  IconPerson,
  IThemeContext,
  Navigation,
  NavigationItem,
  withTheme
} from '../../components';

class PartnersPage extends Component<IThemeContext> {
  public componentDidMount() {
    this.props.toggleTheme('partners');
  }

  public render() {
    return (
      <div className="partners-page">
        <Navigation>
          <NavigationItem
            to="/partners/about"
            icon={<IconInfo />}
            label="О нас"
          />
          <NavigationItem
            to="/partners/news"
            icon={<IconMegaphone />}
            label="Новости"
          />
          <NavigationItem
            to="/partners/new"
            icon={<IconPen />}
            label="Стать партнером"
          />
          <NavigationItem
            to="/partners/learning"
            icon={<IconHat />}
            label="Обучение"
          />
          <NavigationItem
            to="/partners/faq"
            icon={<IconMessage />}
            label="FAQ"
          />
          <NavigationItem
            to="/partners/contacts"
            icon={<IconMarker />}
            label="Контакты"
          />
          <NavigationItem
            to="/partners/personal-area"
            icon={<IconPerson />}
            label="Личный кабинет"
          />
        </Navigation>
        <Switch>
          <Route path="/partners/about" component={ContactsPage} />
          <Route exact path="/partners/news" component={AllNewsPage} />
          <Route path="/partners/news/:id" component={NewsPage} />
          <Route path="/partners/new" component={ContactsPage} />
          <Route path="/partners/learning" component={LearningPage} />
          <Route path="/partners/faq" component={FaqPage} />
          <Route path="/partners/contacts" component={ContactsPage} />
          <Route path="/partners/personal-area" component={ContactsPage} />
        </Switch>
      </div>
    );
  }
}

export default withTheme(PartnersPage);
