import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './StartPage.css';

/* Componets */
import { Button } from '../../components';

/* Images */
import ImageCity from '../../assets/images/city.png';
import ImagePartners from '../../assets/images/partners.svg';
import ImageTourists from '../../assets/images/Tourist.svg';
import ImageTropics from '../../assets/images/tropics.png';

class StartPage extends Component {
  public render() {
    return (
      <section className="start-page">
        <div className="start-page__tourists">
          <div className="start-page__top">
            <img
              className="start-page__background"
              src={ImageTropics}
              alt="Фоновое изображение тропиков"
            />
            <img
              className="start-page__foreground"
              src={ImageTourists}
              alt="Туристы на отдыхе"
            />
          </div>
          <div className="start-page__bottom">
            <Button
              use={Link}
              to="/tourists/faq"
              theme="tourists"
              label="Туристам"
            />
          </div>
        </div>
        {/* <div className="start-page__delimiter" /> */}
        <div className="start-page__partners">
          <div className="start-page__top">
            <img
              className="start-page__background"
              src={ImageCity}
              alt="Фоновое изображение города"
            />
            <img
              className="start-page__foreground"
              src={ImagePartners}
              alt="Изображение партнеров"
            />
          </div>
          <div className="start-page__bottom">
            <Button
              use={Link}
              to="/partners/faq"
              theme="partners"
              label="Партнерам"
            />
          </div>
        </div>
      </section>
    );
  }
}

export default StartPage;
