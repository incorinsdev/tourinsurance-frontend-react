import React, { Component } from 'react';
import Loadable from 'react-loadable';
import { Route, Switch } from 'react-router-dom';
import './TouristsPage.css';

/* Pages */
const AboutPage = Loadable({
  loader: () => import(/* webpackChunkName: "TouristsAboutPage"*/ '../TouristsAboutPage'),
  loading: () => <div>Loading...</div>
});

const ContactsPage = Loadable({
  loader: () => import(/* webpackChunkName: "ContactsPage"*/ '../ContactsPage'),
  loading: () => <div>Loading...</div>
});

const FaqPage = Loadable({
  loader: () => import(/* webpackChunkName: "FaqPage"*/ '../FaqPage'),
  loading: () => <div>Loading...</div>
});

/* Components */
import {
  IconBookmark,
  IconInfo,
  IconMarker,
  IconMessage,
  IconPen,
  IconStar,
  IThemeContext,
  Navigation,
  NavigationItem,
  withTheme
} from '../../components';

class Tourists extends Component<IThemeContext> {
  public componentDidMount() {
    this.props.toggleTheme('tourists');
  }

  public render() {
    return (
      <div className="tourists-page">
        <Navigation>
          <NavigationItem
            to="/tourists/order"
            icon={<IconPen />}
            label="Оформить страховку"
          />
          <NavigationItem
            to="/tourists/rules"
            icon={<IconBookmark />}
            label="Правила страхования"
          />
          <NavigationItem
            to="/tourists/benefits"
            icon={<IconStar />}
            label="Преимущества"
          />
          <NavigationItem
            to="/tourists/about"
            icon={<IconInfo />}
            label="О нас"
          />
          <NavigationItem
            to="/tourists/faq"
            icon={<IconMessage />}
            label="FAQ"
          />
          <NavigationItem
            to="/tourists/contacts"
            icon={<IconMarker />}
            label="Контакты"
          />
        </Navigation>
        <Switch>
          <Route path="/tourists/order" component={ContactsPage} />
          <Route path="/tourists/rules" component={ContactsPage} />
          <Route path="/tourists/benefits" component={ContactsPage} />
          <Route path="/tourists/about" component={AboutPage} />
          <Route path="/tourists/faq" component={FaqPage} />
          <Route path="/tourists/contacts" component={ContactsPage} />
        </Switch>
      </div>
    );
  }
}

export default withTheme(Tourists);
