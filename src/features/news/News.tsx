import React, { Component } from 'react';
import { match } from 'react-router-dom';
import { connect } from 'react-redux';
import './News.css';

/* Components */
import PageHeader from '../../components/PageHeader';

/* Redux */
import { NewsDTO } from '../../services/api/api';
import { getNewsById } from './store/actions';
import { INewsState } from './store/reducer';

interface INewsProps {
  match: match<{
    id: string;
  }>;
  news: NewsDTO;
  getNewsById: any;
}

class News extends Component<INewsProps> {
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.getNewsById(id);
  }

  render() {
    return (
      <section className="news">
        <PageHeader title="Название очереденой новости на сайте" />
        <div className="news-content">
          <img
            className="news-image"
            src={require('../../assets/images/stock-news-image.png')}
          />
          <p>Страховние — отношения (между страхователем и страховщиком) по защите
          имущественных интересов физических и юридических лиц (страхователей)
          при наступлении определённых событий (страховых случаев) за счёт
          денежных фондов (страховых фондов), формируемых из уплачиваемых ими
          страховых взносов (страховой премии).</p>
          <p>Страхование (страховое дело) в широком смысле включает различные виды
          страховой деятельности (собственно страхование, или первичное
          страхование, перестрахование, сострахование, взаимное страхование),
          которые в комплексе обеспечивают страховую защиту.</p>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state: { news: INewsState }) => ({
  news: state.news.selectedNews
});

const mapActionToProps = {
  getNewsById
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(News);
