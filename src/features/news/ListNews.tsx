import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './ListNews.css';

/* Components */
import NewsCard from '../../components/NewsCard';
import PageHeader from '../../components/PageHeader';

/* Redux */
import { NewsDTO } from '../../services/api';
import { INewsState } from './store/reducer';
import { getNews } from './store/actions';

interface IListNewsProps {
  news: NewsDTO[];
  getNews: any;
};

class ListNews extends Component<IListNewsProps> {
  componentDidMount() {
    this.props.getNews(1);
  }

  render() {
    return (
      <section className="list-news">
        <PageHeader title="Будте в курсе последних событий" />
        <div className="list-news__cards">
          <div className="list-news__column">
            <Link to="/partners/news/1">
              <NewsCard
                title="Название очередной новости в несколько строк"
                description="Краткое описание новости на сайт для пояснения"
                date="5 августа 2018"
              />
            </Link>
          </div>
          <div className="list-news__column">
            <Link to="/partners/news/2">
              <NewsCard
                title="Название очередной новости в несколько строк"
                description="Краткое описание новости на сайт для пояснения"
                date="5 августа 2018"
              />
            </Link>
          </div>
          <div className="list-news__column">
            <Link to="/partners/news/3">
              <NewsCard
                title="Название очередной новости в несколько строк"
                description="Краткое описание новости на сайт для пояснения"
                date="5 августа 2018"
              />
            </Link>
          </div>
          <div className="list-news__column">
            <Link to="/partners/news/4">
              <NewsCard
                title="Название очередной новости в несколько строк"
                description="Краткое описание новости на сайт для пояснения"
                date="5 августа 2018"
              />
            </Link>
          </div>
          <div className="list-news__column">
            <Link to="/partners/news/5">
              <NewsCard
                title="Название очередной новости в несколько строк"
                description="Краткое описание новости на сайт для пояснения"
                date="5 августа 2018"
              />
            </Link>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state: { news: INewsState }) => ({
  news: state.news.items
});

const mapActionToProps = {
  getNews
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(ListNews);
