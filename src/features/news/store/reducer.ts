import { NewsDTO } from '../../../services/api/api';
import { GET_NEWS, GET_NEWS_BY_ID } from './action-types';

export interface INewsState {
  readonly items: NewsDTO[];
  readonly selectedNews: NewsDTO;
}

const initialState: INewsState = {
  items: [],
  selectedNews: {
    title: '',
    text: '',
    contentType: 0
  }
};

export default function newsReducer(
  state: INewsState = initialState,
  action: any
) {
  switch (action.type) {
    case GET_NEWS.SUCCESS:
      return {
        ...state,
        items: action.payload
      };
    case GET_NEWS_BY_ID.SUCCESS:
      return {
        ...state,
        selectedNews: action.payload
      };
    default:
      return state;
  }
}
