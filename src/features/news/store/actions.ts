import { Dispatch } from 'redux';
import { NewsApiFp, NewsDTO } from '../../../services/api';
import { GET_NEWS, GET_NEWS_BY_ID } from './action-types';

const newsApi = NewsApiFp();

export type TGetNews = (
  pageNumber: number,
  count?: number
) => Promise<void>;

/**
 * API method for get list news
 *
 * @param pageNumber Requested page
 * @param count Count news in response
 */
export function getNews(pageNumber: number, count: number = 6) {
  const request = newsApi.newsGet(pageNumber, count);

  return async function(dispatch: Dispatch) {
    dispatch({ type: GET_NEWS.PENDING });

    try {
      const news = await request();
      dispatch({ type: GET_NEWS.SUCCESS, payload: news });
    } catch (e) {
      dispatch({ type: GET_NEWS.ERROR });
    }
  };
}

/**
 * API method for get news by id
 *
 * @param id Unique identificator for news
 */
export function getNewsById(id: string) {
  const request = newsApi.newsByIdGet(id);

  return async function(dispatch: Dispatch) {
    dispatch({ type: GET_NEWS_BY_ID.PENDING });

    try {
      const news = await request();
      dispatch({ type: GET_NEWS_BY_ID.SUCCESS, payload: news });
    } catch (error) {
      dispatch({ type: GET_NEWS_BY_ID.ERROR });
    }
  }
}
