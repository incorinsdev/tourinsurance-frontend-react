import { generateTypes } from '../../../lib/actions';

export const GET_NEWS = generateTypes('News', 'GET_NEWS');
export const GET_NEWS_BY_ID = generateTypes('News', 'GET_NEWS_BY_ID');
