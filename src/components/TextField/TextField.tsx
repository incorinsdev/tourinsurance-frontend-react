import classnames from 'classnames';
import React, { Component, FormEvent, SyntheticEvent } from 'react';
import './TextField.css';

interface ITextFieldProps {
  label: string;
  value: string;
  placeholder?: string;
  errorMessage: string;
  onChange?: (event: SyntheticEvent) => void;
  onBlur?: (event: SyntheticEvent) => void;
}

interface ITextFieldState {
  isFocus: boolean;
  value: string;
}

class TextField extends Component<ITextFieldProps, ITextFieldState> {
  public static defaultProps = {
    errorMessage: ''
  };

  public state: ITextFieldState = {
    isFocus: false,
    value: ''
  };

  public inputRef = React.createRef<HTMLInputElement>();

  public handleFocus = () => {
    this.inputRef.current!.focus();
    this.setState({ isFocus: true });
  };

  public handleBlur = () => {
    this.setState({ isFocus: false });
  };

  public handleChange = (event: FormEvent<HTMLInputElement>) => {
    this.setState({ value: event.currentTarget.value });
  };

  public render() {
    const { label, placeholder, errorMessage } = this.props;
    const { isFocus, value } = this.state;
    const isError = errorMessage !== '';

    return (
      <div
        className={
          'Textfield' + (isError ? ' error' : '') + (isFocus ? ' focused' : '')
        }
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        tabIndex={0}
      >
        <label>{label}</label>
        <p
          className={
            (isError ? 'error' : 'placeholder') + (isFocus ? ' focused' : '')
          }
        >
          {errorMessage || (value === '' && placeholder)}
        </p>
        <input ref={this.inputRef} onChange={this.handleChange} />
      </div>
    );
  }
}

export default TextField;
