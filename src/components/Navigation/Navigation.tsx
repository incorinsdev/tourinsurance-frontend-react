import classnames from 'classnames';
import React, { SFC } from 'react';
import './Navigation.css';

/* Components */
import { IThemeContext, withTheme } from '../Theme';

interface INavigationProps {
  theme?: string;
}

const Navigation: SFC<INavigationProps & IThemeContext> = ({
  theme = '',
  children,
}) => {
  const classes = classnames('app-navigation', {
    [`app-navigation--${theme}`]: theme
  });

  return (
    <nav className={classes}>
      {children}
    </nav>
  );
};

export default withTheme(Navigation);
