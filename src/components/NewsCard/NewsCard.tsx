import React, { SFC } from 'react';
import './NewsCard.css';

interface INewsCardProps {
  title: string;
  description: string;
  date: string;
}

const NewsCard: SFC<INewsCardProps> = ({ title, description, date }) => {
  return <div className="app-news-card">
    <header className="app-news-card__title">
      {title}
    </header>
    <div className="app-news-card__description">
      {description}
    </div>
    <footer className="app-news-card__footer">
      <div className="app-news-card__footer-left">
        {date}
      </div>
      {/* <IconButton /> */}
    </footer>
  </div>;
};

export default NewsCard;
