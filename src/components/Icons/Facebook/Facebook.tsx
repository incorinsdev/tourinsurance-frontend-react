import React, { SFC, SVGAttributes } from 'react';
import './Facebook.css';

import IconBase from '../IconBase';

const Facebook: SFC<SVGAttributes<SVGElement>> = props => (
  <IconBase
    className="app-icon__facebook"
    width="30"
    height="30"
    viewBox="0 0 30 30"
    {...props}
  >
    <circle cx="15" cy="15" r="15" />
    <path
      fill="#fff"
      d="M18.31,8.55a9.88,9.88,0,0,0-2.09-.26c-2.24,0-3.13,1.06-3.13,3v1.39h-1.4v2.25h1.4v6.82h2.73V14.89h2L18,12.64H15.82V11.56c0-.61.06-1,.93-1A7.11,7.11,0,0,1,18,10.7Z"
    />
  </IconBase>
);

export default Facebook;
