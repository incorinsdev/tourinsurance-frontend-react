import React, { SFC, SVGAttributes } from 'react';

import IconBase from '../IconBase';

const Pen: SFC<SVGAttributes<SVGElement>> = props => (
  <IconBase
    className="app-icon__pen"
    width="22"
    height="22"
    viewBox="0 0 22 22"
    {...props}
  >
    <path d="M18.8.24a.82.82,0,0,0-1.16,0L6.19,11.69v4.12h4.12L21.76,4.36A.83.83,0,0,0,22,3.78a.78.78,0,0,0-.24-.58ZM9.63,13.75H8.25V12.37l10-10,1.37,1.37Z" />
    <path d="M19.6,10.32a1,1,0,0,0-1,1v8.6H2.06V3.44h8.6a1.67,1.67,0,0,0,.74-.07A1,1,0,0,0,12,2.05a1.09,1.09,0,0,0-1.35-.67H1.37A1.37,1.37,0,0,0,0,2.75V20.62A1.38,1.38,0,0,0,1.38,22H19.25a1.37,1.37,0,0,0,1.37-1.38V11.34A1,1,0,0,0,19.6,10.32Z" />
  </IconBase>
);

export default Pen;
