import classnames from 'classnames';
import React, { SFC, SVGAttributes } from 'react';

const IconBase: SFC<SVGAttributes<SVGElement>> = ({
  children,
  className,
  ...props
}) => {
  const classes = classnames('app-icon', className);

  return (
    <svg className={classes} xmlns="http://www.w3.org/2000/svg" {...props}>
      {children}
    </svg>
  );
};

export default IconBase;
