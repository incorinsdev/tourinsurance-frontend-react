import React, { SFC, SVGAttributes } from 'react';

import IconBase from '../IconBase';

const Mail: SFC<SVGAttributes<SVGElement>> = props => (
  <IconBase
    className="app-icon__mail"
    width="18"
    height="14"
    viewBox="0 0 18 14"
    {...props}
  >
    <path d="M1.13,0A1.1,1.1,0,0,0,0,1.07H0V12.92A1.1,1.1,0,0,0,1.12,14H16.88A1.1,1.1,0,0,0,18,12.93h0V1.08A1.1,1.1,0,0,0,16.88,0H1.13ZM16.31,1.62V2L9,7.91,1.69,2V1.62ZM1.69,12.39V4.15L8.28,9.44a1.14,1.14,0,0,0,1.44,0l6.59-5.29v8.24Z" />
  </IconBase>
);

export default Mail;
