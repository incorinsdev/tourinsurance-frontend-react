import React, { SFC, SVGAttributes } from 'react';

import IconBase from '../IconBase';

const Hat: SFC<SVGAttributes<SVGElement>> = props => (
  <IconBase
    className="app-icon__hat"
    width="24"
    height="18"
    viewBox="0 0 24 18"
    {...props}
  >
    <path d="M22.7,5.14v.14L12.29.14a1.25,1.25,0,0,0-1.16,0L.72,5.28A1.28,1.28,0,0,0,.14,7a1.3,1.3,0,0,0,.58.58l.58.29v4.52a1.29,1.29,0,0,0-.48,1.76,1.32,1.32,0,0,0,1.78.47,1.28,1.28,0,0,0,0-2.23V8.51l1.95,1v4C4.55,16,7.76,18,11.71,18s7.16-2,7.16-4.5v-4l3.83-1.9A1.18,1.18,0,0,0,24,6.43,1.3,1.3,0,0,0,22.7,5.14Zm-11,10.93c-2.93,0-5.2-1.38-5.2-2.57V10.44l4.62,2.28a1.31,1.31,0,0,0,1.16,0l4.62-2.28V13.5h0C16.91,14.69,14.64,16.07,11.71,16.07Zm0-5.22-9-4.42,9-4.42,9,4.42Z" />
  </IconBase>
);

export default Hat;
