import React, { SFC, SVGAttributes } from 'react';
import IconBase from '../IconBase';
import './Cup.css';

const Cup: SFC<SVGAttributes<SVGElement>> = props => (
  <IconBase
    className="Cup"
    width="170"
    height="170"
    viewBox="0 0 170 170"
    {...props}
  >
    <circle className="cls-1" cx="85" cy="85" r="85" />
    <path
      className="cls-2"
      d="M105,126H65V108a2,2,0,0,1,2-2h36a2,2,0,0,1,2,2Z"
    />
    <polygon className="cls-3" points="108 129 62 129 63 126 107 126 108 129" />
    <path
      className="cls-1"
      d="M93,122H77a4,4,0,0,1-4-4v-3a4,4,0,0,1,4-4H93a4,4,0,0,1,4,4v3A4,4,0,0,1,93,122Z"
    />
    <rect className="cls-4" x="79.8" y="84.5" width="10.5" height="16.5" />
    <rect className="cls-5" x="87.2" y="84.5" width="3" height="16.5" />
    <path className="cls-6" d="M94,100H76a6,6,0,0,0-6,6h30A6,6,0,0,0,94,100Z" />
    <path className="cls-7" d="M94,100H89a6,6,0,0,1,6,6h5A6,6,0,0,0,94,100Z" />
    <path
      className="cls-6"
      d="M90.1,93H79.9A4.91,4.91,0,0,1,75,88.1V87H95v1.1A4.91,4.91,0,0,1,90.1,93Z"
    />
    <path
      className="cls-7"
      d="M91,87v1.1c0,2.7-1.2,4.9-3.9,4.9h3A4.91,4.91,0,0,0,95,88.1V87Z"
    />
    <path className="cls-4" d="M102,106H68a2,2,0,0,1,2-2h30a2,2,0,0,1,2,2Z" />
    <path
      className="cls-5"
      d="M100,104H95a2,2,0,0,1,2,2h5A2,2,0,0,0,100,104Z"
    />
    <path className="cls-4" d="M97,85H73v3a2,2,0,0,0,2,2H95a2,2,0,0,0,2-2Z" />
    <path className="cls-4" d="M97,85H73v3a2,2,0,0,0,2,2H95a2,2,0,0,0,2-2Z" />
    <path className="cls-5" d="M92,85l-2,3c1,0,1.1,2,0,2h5a2,2,0,0,0,2-2V85Z" />
    <path className="cls-8" d="M99,53h19v1A19,19,0,0,1,99,73h0" />
    <path className="cls-9" d="M71,53H52v1A19,19,0,0,0,71,73h0" />
    <path
      className="cls-6"
      d="M93.5,88h-17A11.44,11.44,0,0,1,65,76.5V45h40V76.5A11.44,11.44,0,0,1,93.5,88Z"
    />
    <path className="cls-10" d="M71,48V68" />
    <path
      className="cls-7"
      d="M100,45V76.5A11.44,11.44,0,0,1,88.5,88h5A11.44,11.44,0,0,0,105,76.5V45Z"
    />
    <polygon className="cls-4" points="108 45 62 45 65 48 105 48 108 45" />
    <polygon className="cls-5" points="102 45 100 48 105 48 108 45 102 45" />
    <path
      className="cls-6"
      d="M109,41H61a2,2,0,0,0-2,2h0a2,2,0,0,0,2,2h48a2,2,0,0,0,2-2h0A2,2,0,0,0,109,41Z"
    />
    <path className="cls-7" d="M109,41h-6a2,2,0,0,1,0,4h6a2,2,0,0,0,0-4Z" />
    <line className="cls-10" x1="71" y1="72.4" x2="71" y2="74.7" />
    <path
      className="cls-7"
      d="M85,70.5l4.3,2.3a.91.91,0,0,0,1.3-.9L89.7,67l3.5-3.4a.9.9,0,0,0-.5-1.5l-4.8-.7L85.7,57a.85.85,0,0,0-1.5,0L82,61.4l-4.8.7a.9.9,0,0,0-.5,1.5L80.2,67l-.8,4.8a.89.89,0,0,0,1.3.9Z"
    />
  </IconBase>
);

export default Cup;
