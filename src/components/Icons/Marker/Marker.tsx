import React, { SFC, SVGAttributes } from 'react';

import IconBase from '../IconBase';

const Marker: SFC<SVGAttributes<SVGElement>> = props => (
  <IconBase
    className="app-icon__marker"
    width="17"
    height="21"
    viewBox="0 0 17 21"
    {...props}
  >
    <path d="M3.9,1.34A8.33,8.33,0,0,0,1.36,13L7,20.2a1.75,1.75,0,0,0,.53.52A1.78,1.78,0,0,0,10,20.2L15.64,13h0a8.3,8.3,0,0,0,0-9.1A8.55,8.55,0,0,0,3.9,1.34Zm10,10.47L8.5,18.61l-5.36-6.8a6.19,6.19,0,0,1-1-3.41A6.34,6.34,0,0,1,8.5,2.1h0a6.38,6.38,0,0,1,3.45,1A6.24,6.24,0,0,1,13.86,11.81Z" />
    <path d="M8.5,4.9a3.5,3.5,0,1,0,0,7h0a3.5,3.5,0,1,0,0-7Zm0,4.9A1.4,1.4,0,1,1,8.5,7h0a1.4,1.4,0,1,1,0,2.8Z" />
  </IconBase>
);

export default Marker;
