import React, { SFC, SVGAttributes } from 'react';

import IconBase from '../IconBase';

const Message: SFC<SVGAttributes<SVGElement>> = props => (
  <IconBase
    className="app-icon__message"
    width="22"
    height="21"
    viewBox="0 0 22 21"
    {...props}
  >
    <path d="M20.62,0H1.37A1.39,1.39,0,0,0,0,1.4V16.1a1.39,1.39,0,0,0,1.38,1.4H4.13v2.1A1.39,1.39,0,0,0,5.5,21h.69l3.44-3.5h11A1.39,1.39,0,0,0,22,16.1V1.4A1.39,1.39,0,0,0,20.62,0Zm-.68,15.4H8.77L6.19,18V15.4H2.06V2.1H19.94Z" />
  </IconBase>
);

export default Message;
