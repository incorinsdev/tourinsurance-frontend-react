import React, { SFC, SVGAttributes } from 'react';

import IconBase from '../IconBase';

const Info: SFC<SVGAttributes<SVGElement>> = props => (
  <IconBase
    className="app-icon__info"
    width="22"
    height="22"
    viewBox="0 0 22 22"
    {...props}
  >
    <path d="M11,0A11,11,0,1,0,22,11,11,11,0,0,0,11,0Zm0,19.94A8.94,8.94,0,1,1,19.94,11,8.94,8.94,0,0,1,11,19.94Z" />
    <path d="M11,9.63A1.38,1.38,0,0,0,9.62,11v4.12a1.38,1.38,0,1,0,2.75,0V11A1.37,1.37,0,0,0,11,9.63Z" />
    <path d="M11,5.5a1.38,1.38,0,1,0,0,2.75h0A1.38,1.38,0,0,0,11,5.5Z" />
  </IconBase>
);

export default Info;
