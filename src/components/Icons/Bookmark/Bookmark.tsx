import React, { SFC, SVGAttributes } from 'react';

import IconBase from '../IconBase';

const Bookmark: SFC<SVGAttributes<SVGElement>> = props => (
  <IconBase
    className="app-icon__bookmark"
    width="17"
    height="22"
    viewBox="0 0 17 22"
    {...props}
  >
    <path d="M15.58,0H1.42A1.4,1.4,0,0,0,0,1.37V21a1.07,1.07,0,0,0,.14.52,1.08,1.08,0,0,0,1.45.37L8.5,18l6.91,3.83a1.08,1.08,0,0,0,.53.14,1.1,1.1,0,0,0,.53-.14h0A1,1,0,0,0,17,21V1.37A1.4,1.4,0,0,0,15.58,0Zm-.7,19.19L9,16A1.11,1.11,0,0,0,8,16L2.13,19.19V2.06H14.88Z" />
    <rect x="4.25" y="8.25" width="8.5" height="2.06" />
    <rect x="4.25" y="4.13" width="8.5" height="2.06" />
  </IconBase>
);

export default Bookmark;
