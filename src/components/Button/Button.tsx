import classnames from 'classnames';
import React, { ComponentType, ReactNode, SFC } from 'react';
import './Button.css';

import { IThemeContext, withTheme } from '../Theme';

interface IButtonProps {
  use?: ComponentType | string | any;
  icon?: ReactNode;
  theme?: string;
  label: string;
}

const Button: SFC<IButtonProps & IThemeContext & any> = ({
  use,
  icon,
  theme = '',
  label,
  ...props
}) => {
  const Element = use || 'button';
  const classes = classnames('app-button', { [`app-button--${theme}`]: theme });

  const { to } = props;

  return (
    <Element className={classes} to={to}>
      {icon && <span className="app-button__icon">{icon}</span>}
      <span className="app-button__label">{label}</span>
    </Element>
  );
};

export default withTheme(Button);
