import React, { SFC } from 'react';

interface IAppLoaderProps {
  width: number;
}

const AppLoader: SFC<IAppLoaderProps> = ({ width = 0 }) => (
  <div className="app-loader">
    <div className="app-loader__progress" style={{ width: `${width}%` }} />
    <img src={`${process.env.PUBLIC_URL}/logo.svg`} />
  </div>
);

export default AppLoader;
