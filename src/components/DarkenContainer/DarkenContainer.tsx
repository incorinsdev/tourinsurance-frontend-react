import classnames from 'classnames';
import React, { HTMLAttributes, SFC } from 'react';
import './DarkenContainer.css';

/* Images */
import ImageBorder from '../../assets/images/circle-border.png';

const DarkenContainer: SFC<HTMLAttributes<HTMLDivElement>> = ({
  children,
  className,
  ...props
}) => {
  const classes = classnames('app-darken-container', className);

  return (
    <div className={classes} {...props}>
      <div className="app-darken-container__background" />
      <img
        className="app-darken-container-border app-darken-container-border--top"
        src={ImageBorder}
      />
      <div className="app-darken-container__foreground">{children}</div>
      <img
        className="app-darken-container-border app-darken-container-border--bottom"
        src={ImageBorder}
      />
    </div>
  );
};

export default DarkenContainer;
