import classnames from 'classnames';
import React, { HTMLAttributes, SFC } from 'react';
import './DashedContainer.css';

const DashedContainer: SFC<HTMLAttributes<HTMLDivElement>> = ({
  children,
  className,
  ...props
}) => {
  const classes = classnames('app-dashed-container', className);

  return (
    <div className={classes} {...props}>
      {children}
    </div>
  );
};

export default DashedContainer;
