import React, { SFC } from 'react';
import './Footer.css';

/* Components */
import Container from '../Container';
import IconFacebook from '../Icons/Facebook';

const Footer: SFC = () => (
  <Container fluid={true} use="footer" className="app-footer">
    <div className="app-footer__copyright">
      <span>&copy; 2018 Tour-insurance.ru</span>
    </div>
    <div className="app-footer__carousel" />
    <div className="app-footer__social-networks">
      <span>Мы в соц.сетях</span>
      <a><IconFacebook /></a>
    </div>
  </Container>
);

export default Footer;
