import classnames from 'classnames';
import React, { ReactNode, SFC } from 'react';
import { NavLink, NavLinkProps } from 'react-router-dom';
import './NavigationItem.css';

/* Components */
import { IThemeContext, withTheme } from '../Theme';

interface INavigationItemProps {
  icon?: ReactNode;
  theme?: string;
  label: string;
}

const NavigationItem: SFC<
  NavLinkProps & INavigationItemProps & IThemeContext
> = ({ to, icon, theme, label }) => {
  const classes = classnames('app-navigation-item', {
    [`app-navigation-item--${theme}`]: theme
  });

  return (
    <NavLink
      activeClassName="app-navigation-item--active"
      className={classes}
      to={to}
    >
      {icon && <span className="app-navigation-item__icon">{icon}</span>}
      <span className="app-navigation-item__label">{label}</span>
    </NavLink>
  );
};

export default withTheme(NavigationItem);
