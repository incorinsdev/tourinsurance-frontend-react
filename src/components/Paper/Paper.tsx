import classnames from 'classnames';
import React, { SFC } from 'react';
import './Paper.css';

interface IPaperProps {
  className: string;
}

const Paper: SFC<IPaperProps> = ({ children, className }) => {
  const classes = classnames('app-paper', className);
  return <div className={classes}>{children}</div>;
};

export default Paper;
