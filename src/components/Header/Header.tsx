import React, { SFC } from 'react';
import { Link } from 'react-router-dom';
import './Header.css';

/* Components */
import Container from '../Container';
import { IconLogo, IconPhone } from '../Icons';

/* Images */
import ImageLogo from '../../assets/images/logo.svg';

const Header: SFC = () => (
  <Container fluid={true} use="header" className="app-header">
    <div className="app-header__brand">
      <Link to="/">
        <IconLogo />
      </Link>
    </div>
    <div className="app-header__center">
      <p className="app-header__title">
        Страхование имущества на время путешествий
      </p>
      <p className="app-header__subtitle">
        для вашего спокойствия
      </p>
    </div>
    <div className="app-header__contacts">
      <a
        className="app-header__phone"
        href="tel:+74951182324"
      >
        <IconPhone height="35" />
        8 495 118 23 24
      </a>
      <p className="app-header__phone-label">Телефон горячей линии</p>
    </div>
  </Container>
);

export default Header;
