import React, { SFC } from 'react';
import './PageHeader.css';

interface IPageHeaderProps {
  title: string;
}

const PageHeader: SFC<IPageHeaderProps> = ({ title }) => (
  <h2 className="app-page-header">{title}</h2>
);

export default PageHeader;
