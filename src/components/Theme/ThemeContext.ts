import React from 'react';

export type Theme = '' | 'tourists' | 'partners';

export interface IThemeContext {
  theme: Theme;
  toggleTheme: (theme: Theme) => void;
}

export const ThemeContext = React.createContext<IThemeContext>({
  theme: '',
  toggleTheme: (theme: Theme) => void 0
});
