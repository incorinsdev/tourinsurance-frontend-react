import React, { Component } from 'react';
import { getDisplayName } from '../../lib/hoc';
import { IThemeContext, ThemeContext } from './ThemeContext';

const withTheme = <P extends object>(
  WrappedComponent: React.ComponentType<P & IThemeContext>
) => {
  return class extends Component<P> {
    public static displayName = `WithTheme(${getDisplayName(
      WrappedComponent
    )})`;

    public render() {
      return (
        <ThemeContext.Consumer>
          {context => <WrappedComponent {...context} {...this.props} />}
        </ThemeContext.Consumer>
      );
    }
  };
};

export default withTheme;
