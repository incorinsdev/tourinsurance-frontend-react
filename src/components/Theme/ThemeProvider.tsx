import React, { Component } from 'react';
import { Theme, ThemeContext } from './ThemeContext';

interface IState {
  readonly theme: Theme;
}

export class ThemeProvider extends Component<{}, IState> {
  public state: IState = {
    theme: ''
  };

  public toggleTheme = (theme: Theme) => {
    this.setState({ theme });
  };

  public render() {
    const { children } = this.props;
    const { theme } = this.state;

    return (
      <ThemeContext.Provider value={{ theme, toggleTheme: this.toggleTheme }}>
        {children}
      </ThemeContext.Provider>
    );
  }
}
