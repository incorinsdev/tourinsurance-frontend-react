import classnames from 'classnames';
import React, { SFC } from 'react';
import './Container.css';

interface IContainerProps {
  use?: React.ComponentType<any> | string;
  className?: string;
  fluid?: boolean;
}

const Container: SFC<IContainerProps> = ({
  children,
  use,
  className,
  fluid
}) => {
  const Element = use || 'div';
  const classes = classnames(
    'app-container',
    { 'app-container--fluid': fluid },
    className
  );

  return <Element className={classes}>{children}</Element>;
};

export default Container;
