import React, { Component, Fragment } from 'react';
import Loadable from 'react-loadable';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import configureStore from './store';

/* Pages */
import ErrorPage from './features/ErrorPage';

const PartnersPage = Loadable({
  loader: () => import(/* webpackChunkName: "PartnersPage"*/ './features/PartnersPage'),
  loading: () => <div>Loading...</div>
});

const StartPage = Loadable({
  loader: () => import(/* webpackChunkName: "StartPage"*/ './features/StartPage'),
  loading: () => <div>Loading...</div>
});

const TouristsPage = Loadable({
  loader: () => import(/* webpackChunkName: "TouristsPage"*/ './features/TouristsPage'),
  loading: () => <div>Loading...</div>
});

/* Components */
import {
  AppLoader,
  Container,
  Footer,
  Header,
  Paper,
  ThemeProvider
} from './components';

const store = configureStore();

class App extends Component {
  public state = {
    loaded: false,
    loader: 0
  }

  public componentDidMount() {
    setTimeout(() => {
      this.setState({ loader: 100 });
    }, 1000);
    setTimeout(() => {
      this.setState({ loaded: true });
    }, 2000);
  }

  public render() {
    const { loaded, loader } = this.state;

    return (
      <Provider store={store}>
        <ThemeProvider>
          <Router>
            {!loaded ? (
              <AppLoader width={loader} />
            ) : (
              <Fragment>
                <Header />
                <Container use={Paper}>
                  <Switch>
                    <Route exact={true} path="/" component={StartPage} />
                    <Route path="/partners" component={PartnersPage} />
                    <Route path="/tourists" component={TouristsPage} />
                    <Route component={ErrorPage} />
                  </Switch>
                </Container>
                <Footer />
              </Fragment>
            )}
          </Router>
        </ThemeProvider>
      </Provider>
    );
  }
}

export default App;
